<?php
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <script>
    </script>
    <script src="https://www.gstatic.com/firebasejs/5.5.4/firebase.js"></script>
    <script>
        // Initialize Firebase
        var config = {
            apiKey: "AIzaSyBzzj2vV1lrsys5UuRCcww6okMOKrvmtgc",
            authDomain: "myclassflix-prod.firebaseapp.com",
            databaseURL: "https://myclassflix-prod.firebaseio.com",
            projectId: "myclassflix-prod",
            storageBucket: "myclassflix-prod.appspot.com",
            messagingSenderId: "876422344353"
        };
        firebase.initializeApp(config);
        // Generar un random para la sala inicial
        if (!location.hash.replace('#', '').length) {
            location.href = location.href.split('#')[0] + '#' + (Math.random() * 100).toString().replace('.', '');
            location.reload();
        }

        function window_onload() {
            var numberForms = document.forms.length;
            var formIndex;
            for (formIndex = 0; formIndex < numberForms; formIndex++) {
                alert('hola cerrando');
            }
        }
    </script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<style>
    /*stylos video 1*/
    #videos-container video:nth-child(2){
        height: 100% !important;
        width: auto;
        margin-left: auto;
        position: relative;
        /* margin-right: auto; */
        /* transform: scaleX(-1); */
        margin-right: auto;
    }

    /*stylos video 2*/
    #videos-container video:nth-child(3){
        position: absolute;
        /* left: -1px; */
        height: auto;
        width: 14%;
        right: 17px;
        /* bottom: -100%; */
        top: 2%;
        border-radius: 17px;
    }

    #videos-container{
        height: 100%;
        width: 100%;
        display: flex;
    }

    html{
        height: 99%;
        width: 99%;
    }

    body{
        background-color: #292929;
        height: 100%;
        width: 100%;
        margin: 0px;
    }
    
    .logo-myclass{
        height: 39px;
        position: absolute;
        z-index: 10;
        top: 32px;
        left: 32px;
    }

    .backgrounMensage{
        height: 100%;
        width: 100%;
        background-color: #000000a1;
        position: absolute;
        display: flex;
        justify-content: center;
    }

    #openRoom{
        padding: 13px;
        background-color: transparent;
        border-style: none;
        border: 2px solid #fff;
        border-radius: 44px;
        color: white;
        font-weight: 600;
        font-size: 26px;
        padding-left: 50px;
        padding-right: 50px;
        margin-left: auto;
        position: relative;
        margin-right: auto;
        width: auto;
    }
    .center{
        padding: 2px;
        display: flex;
        margin-top: 26px;
    }

</style>

<body>
<?php
    // if ($_GET['user'] == 'teacher') {
?>
    
<?php
    // }
?>
<!-- <div id="modal-initiate" class="backgrounMensage">
    <div style="margin-top: 85px">
    <div class="center">
        <img style="margin-left: auto;margin-right: auto;position: relative;width: 203px;height: auto;" src="logo-init.png">
    </div>
    <div class="center">
        <h2 style="color: white;font-family: sans-serif;">PUEDES INICIAR TU CLASE AHORA</h2>
    </div>
    <div class="center">
        <button id="openRoom">INICIAR CLASE</button>
    </div>
    </div>
</div> -->

<!-- <div id="modal-initiate" class="backgrounMensage">
    <div style="margin-top: 85px">
        <div class="center">
            <img style="margin-left: auto;margin-right: auto;position: relative;width: 203px;height: auto;" src="logo-init.png">
        </div>
        <div class="center">
            <h2 style="color: white;font-family: sans-serif;">ESPERA A QUE TU PROFESOR SE CONECTE</h2>
        </div>
    </div>
</div> -->

    <button id="joinRoom">Conectar</button>
    <button id="shareScreen" disabled style="display:none">Compartir pantalla</button>
    <button id="recordAudioVideo" disabled>grabar</button>
    <button id="mute">quitar micrófono</button>
    <button id="deleteControls"> quitar controles</button>
    <div id="recorded-videos"></div>
    <div id="videos-container">
        <img class="logo-myclass" src="logo.png" >
    </div>
    <!-- 
        Librerías externas para la conexión con el api webrtc
     -->
    <script src="https://www.webrtc-experiment.com/RecordRTC.js"></script>
    <script src="https://cdn.webrtc-experiment.com/socket.io.js"></script>
    <script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
    <script src="https://cdn.webrtc-experiment.com/CodecsHandler.js"></script>
    <script src="https://cdn.webrtc-experiment.com/IceServersHandler.js"></script>
    <script src="https://cdn.webrtc-experiment.com/RTCMultiConnection.js"></script>


    <!-- 
        Configuración y estilo propio para el sistema
     -->
    <script>
        var rmc = new RTCMultiConnection();
        var SIGNALING_SERVER = 'https://socketio-over-nodejs2.herokuapp.com:443/';
        rmc.openSignalingChannel = function (config) {
            var channel = config.channel || rmc.channel || 'default-namespace';
            var sender = Math.round(Math.random() * 9999999999) + 9999999999;

            io.connect(SIGNALING_SERVER).emit('new-channel', {
                channel: channel,
                sender: sender
            });

            var socket = io.connect(SIGNALING_SERVER + channel);
            socket.channel = channel;

            socket.on('connect', function () {
                if (config.callback) config.callback(socket);
            });

            socket.send = function (message) {
                socket.emit('message', {
                    sender: sender,
                    data: message
                });
            };

            socket.on('message', config.onmessage);
        };

        rmc.body = document.getElementById('videos-container');
        // http://www.rtcmulticonnection.org/docs/#getExternalIceServers
        rmc.getExternalIceServers = false;

        document.getElementById('openRoom').onclick = function () {
            this.disabled = true;
            // ocultar opacidad
            document.getElementById('modal-initiate').style.display = 'none'
            // http://www.rtcmulticonnection.org/docs/open/
            rmc.open();
        };

        document.getElementById('joinRoom').onclick = function () {
            // firebase.database().ref('users/').set({
            //         username: 'name',
            //         email: 'email',
            //         profile_picture : 'pic'
            //     }).then(()=>{
            //     })
            // this.disabled = true;
            // http://www.rtcmulticonnection.org/docs/connect/
            rmc.connect();
        };

        document.getElementById('mute').onclick = function () {
            rmc.streams.mute({
                audio: false,
                video: false,
                type: 'remote' // all remote streams
            });
        };

        document.getElementById('deleteControls').onclick = function () {
            var videos = document.getElementsByTagName('video'); 
            for(var i=0; i<videos.length; i++) {
                var video = videos[i].controls = false;
            }
        }

        window.onbeforeunload = function () {
            // Firefox
            document.getElementById('openRoom').disabled = false;
            document.getElementById('joinRoom').disabled = false;
        };

        rmc.onMediaCaptured = function () {
            document.getElementById('openRoom').disabled = true;
            document.getElementById('joinRoom').disabled = true;
        };

        rmc.onclose = function (event){
        }

        rmc.onleave = function (event){
            document.getElementById('recordAudioVideo').click();
        }

        //se ejecuta cuando los dos usuarios ya tienen una interaccion
        rmc.onstream = function (event) {
            document.getElementById('recordAudioVideo').click();
            rmc.body.appendChild(event.mediaElement);

            if (event.type === 'remote' && !recorders.length) {
                document.getElementById('shareScreen').disabled = false;
                document.getElementById('recordAudioVideo').disabled = false;
            }

            setTimeout(() => {
                var videos = document.getElementsByTagName('video'); 
                for(var i=0; i<videos.length; i++) {
                    var video = videos[i].controls = false;
                }  
            }, 1000); 
        };

        document.getElementById('shareScreen').onclick = function () {
            this.disabled = true;

            // http://www.rtcmulticonnection.org/docs/addStream/
            rmc.addStream({
                screen: true,
                oneway: true
            });
        };

        var recorders = [];
        document.getElementById('recordAudioVideo').onclick = function () {
            var streams = rmc.streams.selectAll({
                local: true
            });

            streams = streams.concat(rmc.streams.selectAll({
                remote: true
            }));

            var button = this;

            if (button.innerHTML == 'grabar') {
                button.disabled = true;

                streams.forEach(function (streamEvent) {
                    var recorder = RecordRTC(streamEvent.stream, {
                        type: 'video'
                    });

                    recorder.startRecording();

                    recorders.push(recorder);
                });

                setTimeout(function () {
                    button.innerHTML = 'dejar de grabar'; 
                    button.disabled = false;
                }, 3000);

            } else if (button.innerHTML == 'dejar de grabar') {
                recorders.forEach(function (recorder) {
                    recorder.stopRecording(function () {
                        appendRecordedVideo(recorder.blob);
                    });
                });

                recorders = [];
                button.innerHTML = 'grabar';
            }
        };
        

        var recordedVideos = document.getElementById('recorded-videos');
        function appendRecordedVideo(blob) {
            var d = new Date()
            firebase.storage().ref('aaa/archivo' + d.getTime()).put(blob).then(()=>{console.log('ya quedó en el storage')})
            if (blob.video) {
                blob = blob.video;
            }
            var video = document.createElement('video');
            video.controls = true;
            video.src = URL.createObjectURL(blob);
            recordedVideos.appendChild(video);
        }
        
    </script>
</body>
</html>

<?php

/**
 * Validar el tipo de usuario |Estudiante|Profesor| 
 */
if ($_GET['user'] == 'teacher') {
        // Si soy profesor
    echo "
        <script>
            setTimeout(() => {
                document.getElementById('openRoom').click()
            }, 3000);
        </script> 
        ";
} else {
        // Si soy estudiante
    echo "
        <script>
            document.getElementById('joinRoom').click()
        </script>
        ";
}

?>